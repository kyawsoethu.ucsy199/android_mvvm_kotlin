package com.frontiir.myapplication.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment" +
                "A little over two years ago, I was working on Android for Beginners; a class that takes students from zero programming to their first Android app. As part of the course, students build a very simple one screen app called Court-Counter.\n" +
                "Court-Counter is a very straightforward app with buttons that modify a basketball score. The finished app has a bug though; if you rotate the phone, your current score will inexplicably disappear."
    }
    val text: LiveData<String> = _text
}