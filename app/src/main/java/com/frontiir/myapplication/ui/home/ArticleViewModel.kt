package com.frontiir.myapplication.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.frontiir.myapplication.model.ArticleResponse
import com.frontiir.myapplication.repository.ArticleRepository

class ArticleViewModel(application: Application) : AndroidViewModel(application) {
    private val articleRepository: ArticleRepository
    val articleResponseLiveData: LiveData<ArticleResponse?>

    init {
        articleRepository = ArticleRepository()
        articleResponseLiveData =
            articleRepository.getMovieArticles("movies", "84a7decf3110498ea372bd16dd0601e8")
    }
}